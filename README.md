# deemix-playlist-cron
Docker image built on top of [deemix](https://deemix.app/) to  periodically download all user's playlists.

## Usage
1. Create `config` directory used by [deemix](https://deemix.app/). The easies way is to use [deemix-gui](https://gitlab.com/RemixDev/deemix-gui) and copy it from `/config/` inside the container.
2. Extract your `USER_ID` from `deezer`. It looks like `4042812442` and you can get it from your profile URL - `https://www.deezer.com/en/profile/4042812442/playlists`.
3. Determine how frequent you want to download music for `FREQUENCY` env. The default value is `1d`. Script adds random number of minutes in range `15-59` to this value.
   > **ProTip:** Set it to at least **1h** to prevent Deezer banning this method and use `"overwriteFile": "n"` in `config.json`.
4. Run container!
```
docker run -v /path/to/config:/app/config -v /path/to/your/music:/downloads -e USER_ID=<Your_user_id> -e FREQUENCY=1d registry.gitlab.com/krzystooof/deemix-playlist-cron
```

```
version: "3.3"
services:
  deemix-playlist-cron:
    image: registry.gitlab.com/krzystooof/deemix-playlist-cron
    environment:
      - USER_ID=<Your_user_id>
      - FREQUENCY=1d
    volumes:
      - /path/to/config:/app/config
      - /path/to/your/music:/downloads
    restart: unless-stopped
```
