FROM python:alpine

RUN apk add jq curl gcc musl-dev
RUN python3 -m pip install deemix

ENV USER_ID=""
ENV FREQUENCY="1d"

VOLUME ["/app"]
VOLUME ["/downloads"]

ADD entrypoint.sh /app/

WORKDIR /app

CMD ./entrypoint.sh ${USER_ID} ${FREQUENCY}
