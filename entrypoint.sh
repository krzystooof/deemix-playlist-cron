#!/bin/sh

USER_ID=$1
FREQUENCY=$2

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    exit 1
fi

while true
do
    PLAYLISTS_URLS=$(curl -s  https://api.deezer.com/user/$USER_ID/playlists | jq -r '.data | .[] | .link');
    for uri in $PLAYLISTS_URLS; do
        deemix -p /downloads --portable "$uri"
    done 
    sleep_time="$FREQUENCY $(shuf -i 15-59 -n 1)m"
    echo "Waiting $sleep_time..."
    sleep $sleep_time
done
